class Father {
    constructor(name = 'Darth') {
        this.name = name.toUpperCase();
    }
    get myName() {
        return this.name;
    }
}
const father1 = new Father();
console.log(father1.myName);

class Son extends Father {
    constructor(name = 'Luke') {
        super();
        this.sonsName = name;
    }
    get myName() {
        return `${this.sonsName} ${this.name}'s son`;
    }
}
const son1 = new Son();
console.log(son1.myName);
